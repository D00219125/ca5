package Ca5Main;

import DTOs.TollEvent;
import Daos.ITollEventDaoInterface;
import Daos.MySQLTollEventDAO;
import Exceptions.DaoException;

import java.io.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    ArrayList<TollEvent> validTollEventArrayList = new ArrayList<TollEvent>();
    ArrayList<TollEvent> invalidTollEventArrayList = new ArrayList<TollEvent>();
    public Scanner kb = new Scanner(System.in);
    public void menu()
    {
        int input = 0;
        while(input != 5)
        {
            try
            {
                printOptions();
                input = kb.nextInt();
                switch (input)
                {
                    case 1:
                        validateVehiclesCsv();
                        break;
                    case 2:
                        validateAndStoreTollEventsCsv();
                        break;
                    case 3:
                        batchJob();
                        break;
                    case 4:
                        daoMenu();
                        break;
                }
            }catch (InputMismatchException e)
            {
                System.out.println("You messed up the input, I'm not impressed.");
            }
            System.out.println("enter something to continue");
            kb.next();
        }
    }

    private void daoMenu() {
        int input = 0;
        while(input != 7)
        {
            try
            {
                printDaoOptions();
                input = kb.nextInt();
                switch (input)
                {
                    case 1:
                        getAllTollEvents();
                        break;
                    case 2:
                        getAllTollEventsForAReg();
                        break;
                    case 3:
                        getTollEventsFromADate();
                        break;
                    case 4:
                        getTollEventsBetweenDates();
                        break;
                    case 5:
                        getRegistrations();
                        break;
                    case 6:
                        getAllTollEventsAsMap();
                        break;
                }
            }catch (InputMismatchException e)
            {
                System.out.println("You messed up the input, I'm not impressed.");
            }
        }
    }

    private void getAllTollEvents() {
        //my program doesn't work when I declare one of these at the top, I know it shouldn't but I haven't figured out why it happens
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            tollEventDaoInterface.findAllTollEvents();
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        }
    }

    private void getAllTollEventsForAReg() {
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            System.out.println("Enter the registration you would like to see the toll events for");
            String reg = kb.nextLine();
            tollEventDaoInterface.findEventsByRegistration(reg);
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        } catch (InputMismatchException e)
        {
            System.out.println("You messed up the input, I'm not impressed.");
        }
    }

    private void getTollEventsFromADate() {
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            System.out.println("Enter the date you would like to start reading tollEvents from in format YYYY-MM-DD");
            String date = kb.nextLine();
            tollEventDaoInterface.findEventsSinceDate(Instant.parse(date));
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        } catch (InputMismatchException e)
        {
            System.out.println("You messed up the input, I'm not impressed.");
        }
    }

    private void getTollEventsBetweenDates() {
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            System.out.println("Enter the date you would like to start reading tollEvents from in format YYYY-MM-DD");
            String startDate = kb.nextLine();
            System.out.println("Enter the date you would like to end reading tollEvents from in format YYYY-MM-DD");
            String endDate = kb.nextLine();
            tollEventDaoInterface.findEventsBetweenDates(Instant.parse(startDate), Instant.parse(endDate));
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        } catch (InputMismatchException e)
        {
            System.out.println("You messed up the input, I'm not impressed.");
        }
    }

    private void getRegistrations() {
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            ArrayList<TollEvent> sortedRegList = tollEventDaoInterface.findAllRegistrations();
            for(TollEvent t: sortedRegList)
            {
                System.out.println(t.getVehicleReg());
            }
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        }
    }

    private void getAllTollEventsAsMap() {
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            tollEventDaoInterface.getAllRegistrationsAsMap();
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        }
    }

    private void printDaoOptions() {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Enter 1 to get all toll event details");
        System.out.println("Enter 2 to get all toll details given a vehicle registration");
        System.out.println("Enter 3 to get all toll details since a certain date");
        System.out.println("Enter 4 to get all toll events between two dates");
        System.out.println("Enter 5 to get all registrations that passed the toll in alphabetical order");
        System.out.println("Enter 6 to get all toll events as Map<registration, List<TollEvents>");
        System.out.println("Enter 7 to quit.");
        System.out.println("--------------------------------------------------------------------------------");
    }

    private void batchJob() {
        ITollEventDaoInterface tollEventDaoInterface = new MySQLTollEventDAO();
        try {
            tollEventDaoInterface.batchJob(validTollEventArrayList);
        } catch (DaoException e) {
            System.out.println("I messed up the Dao's");
        }
    }


    private void validateAndStoreTollEventsCsv() {
        //Do you mean store in database or collection?
        //ITollEventDaoInterface ITollDao = new MySQLTollEventDAO();

        String space = "";
        String meantToBeComma = ";";
        try
        {
            //read toll events file
            BufferedReader br = new BufferedReader(new FileReader("Toll-Events.csv"));
            while((space = br.readLine()) != null)
            {
                String[] input = space.split(meantToBeComma);
                String reg = input[0];
                long img = Long.parseLong(input[1]);
                Instant inst = Instant.parse(input[2]);
                TollEvent t = new TollEvent(reg, img, inst);
                ValidRegCheck(t);
                if(t.isValid())
                {
                    validTollEventArrayList.add(t);
                }
                else
                {
                    invalidTollEventArrayList.add(t);
                }


            }

        }
        catch (FileNotFoundException e)
        {
            System.out.println("I messed up the file path again");
        }
        catch (IOException e) {
            System.out.println("I don't know how this exception happens.");
        }

    }

    //don't know if you want me to add them to the database or not. It doesn't say in the brief

    private void validateVehiclesCsv() {
        try
        {
            Scanner sc = new Scanner(new File("Vehicles.csv"));
            //sc.useDelimiter(","); Why doesn't .csv mean comma separated values?!
            while(sc.hasNextLine()){
                String reg = sc.nextLine();
                TollEvent t = new TollEvent(reg);
                ValidRegCheck(t);

                //I'm just going to assume you meant for us to add these to our collection
                if(t.isValid())
                {
                    validTollEventArrayList.add(t);
                }
                else
                {
                    invalidTollEventArrayList.add(t);
                }

            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Well I messed up the file path, well done me");
        }
    }

    private void printOptions() {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Enter 1 to validate registrations from Vehicles.csv");
        System.out.println("Enter 2 to validate and store vehicles from Toll-Events.csv");
        System.out.println("Enter 3 to do a batch job(whatever that is)");
        System.out.println("Enter 4 to access part b's DAO Function menu*");
        System.out.println("Enter 5 to quit.");
        System.out.println("--------------------------------------------------------------------------------");
    }

//    private enum Choice
//    {
//        LOAD_AND_VALIDATE_VEHICLES_CSV(1), VALIDATE_AND_STORE_VEHICLES_FROM_TOLL_EVENTS_CSV(2),
//        BATCH_JOB(2), QUIT(4);
//        boolean exists(int val)
//        {
//            Choice[]choiceArr = values();
//            for(Choice c : choiceArr)
//            {
//                if(c.compareTo(val) == true){}
//            }
//        }
//        Choice(int i) {
//
//        }
//    }
    private void ValidRegCheck(TollEvent t)
    {
        if(t.isValid())
        {
            System.out.println("Vehicle registration: " + t.getVehicleReg() + " is valid");
        }
        else
        {
            System.out.println("Vehicle registration: " + t.getVehicleReg() + " is invalid");
        }
    }
}
