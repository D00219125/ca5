package DTOs;

/*
    source for british regex https://gist.github.com/danielrbradley/7567269
    source for irish regex https://stackoverflow.com/questions/20070387/java-regex-pattern-matching-irish-car-registration
 */

import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TollEvent
{

    private String vehicleReg;
    //use .intvalue() to return as an int
    private long imageID = 0;
    private Instant timeStamp;
    private boolean valid;

    public TollEvent(String vehicleReg) {
        this.vehicleReg = vehicleReg;
        this.imageID = ++imageID;
        this.timeStamp = Instant.now();
        this.valid = isRegValid(vehicleReg);
    }

    public TollEvent(String vehicleReg, long imageID, Instant timeStamp) {
        this.vehicleReg = vehicleReg;
        this.imageID = imageID;
        this.timeStamp = timeStamp;
        this.valid = isRegValid(vehicleReg);
    }

    private boolean isRegValid(String input)
    {
        String validIrishRegRegex = "(?<Current>^[A-Z]{2}[0-9]{2}[A-Z]{3}$)|(?<Prefix>^[A-Z][0-9]{1,3}[A-Z]{3}$)|(?<Suffix>^[A-Z]{3}[0-9]{1,3}[A-Z]$)|(?<DatelessLongNumberPrefix>^[0-9]{1,4}[A-Z]{1,2}$)|(?<DatelessShortNumberPrefix>^[0-9]{1,3}[A-Z]{1,3}$)|(?<DatelessLongNumberSuffix>^[A-Z]{1,2}[0-9]{1,4}$)|(?<DatelessShortNumberSufix>^[A-Z]{1,3}[0-9]{1,3}$)|(?<DatelessNorthernIreland>^[A-Z]{1,3}[0-9]{1,4}$)|(?<DiplomaticPlate>^[0-9]{3}[DX]{1}[0-9]{3}$)";
        String validBritishRegRegex =    "^(\\\\d{2}-?(KK|kk|ww|WW|c|C|ce|CE|cn|CN|cw|CW|d|D|dl|DL|g|G|ke|KE|ky|KY|l|L|ld|LD|lh|LH|lk|LK|lm|LM|ls|LS|mh|MH|mn|MN|mo|MO|oy|OY|so|SO|rn|RN|tn|TN|ts|TS|w|W|wd|WD|wh|WH|wx|WX)-?\\\\d{1,4})$";
        Pattern irishPattern = Pattern.compile(validIrishRegRegex);
        Pattern britishPattern = Pattern.compile(validBritishRegRegex);
        Matcher matcher = irishPattern.matcher(input);
        if(matcher.matches()) return true;
        matcher = britishPattern.matcher(input);
        if(matcher.matches()) return true;
        return false;
    }

    public String getVehicleReg() {
        return vehicleReg;
    }

    public long getImageID() {
        return imageID;
    }

    public Instant getTimeStamp() {
        return timeStamp;
    }

    public boolean isValid() {
        return valid;
    }

    @Override
    public String toString() {
        return "TollEvent{" +
                "vehicleReg='" + vehicleReg + '\'' +
                ", imageID=" + imageID + ".jpg"+
                ", timeStamp=" + timeStamp +
                '}';
    }


//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }
}
