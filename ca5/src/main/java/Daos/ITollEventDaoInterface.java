package Daos;
/* Declares the methods that all userDAO types must implement
they could be mySQL, Mongo, Postgres

Classes in my Business Layer should use a reference to the interface type
to avoid dependencies on the underlying concrete classes
 */

import DTOs.TollEvent;
import Exceptions.DaoException;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ITollEventDaoInterface
{
    public void batchJob(ArrayList <TollEvent> tollEventArrayList) throws DaoException;
    public List<TollEvent> findAllTollEvents() throws DaoException;
    public TollEvent findEventsByRegistration(String registration) throws DaoException;
    public void findEventsSinceDate(Instant i)throws DaoException;//should I parse the inputs before or after they're in the method?
    public void findEventsBetweenDates(Instant start, Instant end)throws DaoException;
    public ArrayList<TollEvent> findAllRegistrations()throws DaoException;
    public Map<String ,ArrayList<TollEvent>> getAllRegistrationsAsMap()throws DaoException;


}
