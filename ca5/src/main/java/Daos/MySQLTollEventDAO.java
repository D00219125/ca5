package Daos;

import DTOs.TollEvent;
import Exceptions.DaoException;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;

public class MySQLTollEventDAO extends MySqlDao implements ITollEventDaoInterface
{
    //list of invalid Tolls to be reviewed by human
    public List<TollEvent> invalidVehicles = new ArrayList<>();


    @Override
    public void batchJob(ArrayList<TollEvent> tollEventArrayList) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = this.getConnection();
            for(int i = 0; i<tollEventArrayList.size();i++)
            {
                //ps=con.createStatement();
                ps.executeUpdate("insert into gd2_toll_event_database.REGISTRATION values (" +tollEventArrayList.get(i).getVehicleReg()+")");
                ps.executeUpdate("insert into gd2_toll_event_database.IMAGEID values (" +tollEventArrayList.get(i).getImageID()+")");
                ps.executeUpdate("insert into gd2_toll_event_database.TIMESTAMP values (" +tollEventArrayList.get(i).getTimeStamp().toString()+")");
            }
            System.out.println("BatchJob: Tolls added to Database");
        } catch (SQLException e) {
            throw new DaoException("batchJob SQL Exception: " + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }
        }
    }


    @Override
    public List<TollEvent> findAllTollEvents() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList<>();
        try {
            con = this.getConnection();

            String query = "select * from gd2_toll_event_database";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                String registration = rs.getString("vehicleReg");
                long image = rs.getLong("image");
                Instant timestamp = rs.getTimestamp("timestamp").toInstant();
                Boolean valid = rs.getBoolean("isValid");
                TollEvent t = new TollEvent(registration, image, timestamp);
                if (!t.isValid()) {
                    invalidVehicles.add(t);
                }
                tollEvents.add(t);
                //removethis
                System.out.println("Stuff added");
            }
        }
        catch(SQLException e)
            {
                throw new DaoException("tollEvents() " + e.getMessage());
            }
        finally
            {
                try
                {
                    if(rs != null)
                    {
                        rs.close();
                    }
                    if(ps != null)
                    {
                        ps.close();
                    }
                    if(con != null)
                    {
                        freeConnection(con);
                    }
                }
                catch(SQLException e)
                {
                    throw new DaoException("findAllUsers() " + e.getMessage());
                }
            }
            return tollEvents;
    }

    @Override
    public TollEvent findEventsByRegistration(String registration) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        TollEvent t = null;

        try
        {
            con = this.getConnection();

            String query = "select * from gd2_toll_event_database where registration = "+registration;
            ps = con.prepareStatement(query);
            ps.setString(1, registration);

            rs = ps.executeQuery();

            if(rs.next())
            {
                String reg = rs.getString("Registration");
                long img = rs.getLong("ImageId");
                String timeStamp = rs.getString("TimeStamp");
                t = new TollEvent(reg, img, Instant.parse(timeStamp));
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findUserByRegistration() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findByRegistration() " + e.getMessage());
            }

        }
        return t;
    }

    @Override
    public void findEventsSinceDate(Instant i) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        TollEvent t = null;

        try
        {
            con = this.getConnection();

            String query = "select * from gd2_toll_event_database where TimeStamp > " +i.toString();
            ps = con.prepareStatement(query);
            ps.setString(1, i.toString());

            rs = ps.executeQuery();

            if(rs.next())
            {
                String reg = rs.getString("Registration");
                long img = rs.getLong("ImageId");
                String timeStamp = rs.getString("TimeStamp");
                t = new TollEvent(reg, img, Instant.parse(timeStamp));
                System.out.println(t.toString());
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findUserByRegistration() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findByRegistration() " + e.getMessage());
            }
        }
    }

    @Override
    public void findEventsBetweenDates(Instant start, Instant finish) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        TollEvent t = null;

        try
        {
            con = this.getConnection();

            String query = "select * from gd2_toll_event_database where TimeStamp > " +start.toString() + " and " + finish.toString();
            ps = con.prepareStatement(query);
            ps.setString(1, start.toString());
            ps.setString(2,finish.toString());

            rs = ps.executeQuery();

            if(rs.next())
            {
                String reg = rs.getString("Registration");
                long img = rs.getLong("ImageId");
                String timeStamp = rs.getString("TimeStamp");
                t = new TollEvent(reg, img, Instant.parse(timeStamp));
                System.out.println(t.toString());
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findUserByRegistration() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findByRegistration() " + e.getMessage());
            }
        }
    }

    @Override
    public ArrayList<TollEvent> findAllRegistrations() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TollEvent> tollEvents = new ArrayList<>();
        try {
            con = this.getConnection();

            String query = "select Registration from gd2_toll_event_database";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                String registration = rs.getString("vehicleReg");
                long image = rs.getLong("image");
                Instant timestamp = rs.getTimestamp("timestamp").toInstant();
                TollEvent t = new TollEvent(registration, image, timestamp);
                tollEvents.add(t);
            }

        }
        catch(SQLException e)
        {
            throw new DaoException("tollEvents() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }
        }
        Collections.sort(tollEvents, new Comparator<TollEvent>() {
            @Override
            public int compare(TollEvent o1, TollEvent o2) {
                return o1.getVehicleReg().compareToIgnoreCase(o2.getVehicleReg());
            }
        });
        return tollEvents;
    }

    @Override
    public Map<String, ArrayList<TollEvent>> getAllRegistrationsAsMap() throws DaoException {
        Map<String, ArrayList<TollEvent>> map = new Map<String, ArrayList<TollEvent>>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean containsKey(Object key) {
                return false;
            }

            @Override
            public boolean containsValue(Object value) {
                return false;
            }

            @Override
            public ArrayList<TollEvent> get(Object key) {
                return null;
            }

            @Override
            public ArrayList<TollEvent> put(String key, ArrayList<TollEvent> value) {
                return null;
            }

            @Override
            public ArrayList<TollEvent> remove(Object key) {
                return null;
            }

            @Override
            public void putAll(Map<? extends String, ? extends ArrayList<TollEvent>> m) {

            }

            @Override
            public void clear() {

            }

            @Override
            public Set<String> keySet() {
                return null;
            }

            @Override
            public Collection<ArrayList<TollEvent>> values() {
                return null;
            }

            @Override
            public Set<Entry<String, ArrayList<TollEvent>>> entrySet() {
                return null;
            }

            @Override
            public boolean equals(Object o) {
                return false;
            }

            @Override
            public int hashCode() {
                return 0;
            }
        };
        List<TollEvent> list = findAllTollEvents();
        for(int i = 0; i<list.size(); i++)
        {
            if(!map.containsKey(list.get(i).getVehicleReg()))
            {
                ArrayList<TollEvent> l = new ArrayList<TollEvent>() {};
                l.add(list.get(i));
                map.put(list.get(i).getVehicleReg(), l);
            }
            else
            {
                ArrayList<TollEvent> l= map.get(list.get(i));
                l.add(list.get(i));
                map.replace(list.get(i).getVehicleReg(), l);
            }
        }
        return map;
    }
}
