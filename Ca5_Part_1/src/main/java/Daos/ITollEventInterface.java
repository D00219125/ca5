package Daos;

import Dtos.TollEvent;
import Exceptions.DaoException;

import java.util.List;

public interface ITollEventInterface {
    public List<TollEvent>  tollEvents() throws DaoException;
    public TollEvent findCarByReg(String registration);
}
