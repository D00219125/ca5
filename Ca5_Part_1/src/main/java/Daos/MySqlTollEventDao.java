package Daos;

import Dtos.TollEvent;
import Exceptions.DaoException;
//import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

//source for british regex https://gist.github.com/danielrbradley/7567269
//source for irish regex https://stackoverflow.com/questions/20070387/java-regex-pattern-matching-irish-car-registration
//i picked a random 40 possible polish plate plate prefix's bacause as far as I can tell there are around 400 prefixes
/*
* -----------OBJECTIVES for database--------------
* 1)    Check if reg is valid and add to a list (if not add to a seperate list)
* 2)    at 4AM add all toll events to the database
* */
public class MySqlTollEventDao extends MySqlDao implements ITollEventInterface{
    //don't know why we need these when an isvalid boolean would do
    private List<TollEvent> invalidVehicles = new ArrayList<>();

    @Override
    public List<TollEvent> tollEvents() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList<>();
        try
        {
            con = this.getConnection();

            String query = "select * from gd2_toll_event_database";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            //the way I think you want it
            while(rs.next())
            {
                String registration = rs.getString("vehicleReg");
                long image = rs.getLong("image");
                Instant timestamp = rs.getTimestamp("timestamp").toInstant();
                Boolean valid = rs.getBoolean("isValid");
                TollEvent t = new TollEvent(registration, image, timestamp);
                if(!t.isValid(registration)){invalidVehicles.add(t);}
                tollEvents.add(t);
                //removethis
                System.out.println("Stuff added");
            }
            //the way I would do it
//            while(rs.next())
//            {
//                String registration = rs.getString("vehicleReg");
//                TollEvent t = new TollEvent(registration);
//                if(!t.isValid(registration)){invalidVehicles.add(t);}
//                tollEvents.add(t);
//            }
        }
        catch(SQLException e)
        {
            throw new DaoException("tollEvents() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }
        }
        return tollEvents;
    }


    @Override
    public TollEvent findCarByReg(String registration) {
        return null;
    }

}
