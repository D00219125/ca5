package BuisnessObjects;

import Daos.ITollEventInterface;
import Daos.MySqlTollEventDao;
import Dtos.TollEvent;
import Exceptions.DaoException;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ITollEventInterface iTollDao = new MySqlTollEventDao();
        try {
            //list all toll events
            List<TollEvent> events = iTollDao.tollEvents();

            if(events.isEmpty()){ System.out.println("List is empty"); }

            for(TollEvent event: events)
            {
                System.out.println("event: " + event);
            }

            //find a car by registration
            TollEvent findEvent = iTollDao.findCarByReg("16D001");
            if(findEvent!= null){
                System.out.println("event(s) found: " + findEvent);
            }
            else{System.out.println("Registration not found");}
        }
        catch (DaoException e) {System.out.println(e.getMessage()); }
    }
}
